# !/usr/bin/python3
import random

import cv2
from PySide import QtCore
from PIL import Image
import zbarlight

qr_codes = []


class SimpleTestThread(QtCore.QThread):
    def __init__(self):
        super(SimpleTestThread, self).__init__()
        self.cam = cv2.VideoCapture(-1)
        self._file_name = 'qr_code.jpg'

    def add_to_list(self, res):
        if res[0] not in qr_codes:
            qr_codes.extend(res)

    def run(self):
        while True:
            ret, frame = self.cam.read()
            cv2.imwrite(self._file_name, frame)
            with open(self._file_name, 'rb') as image_file:
                image = Image.open(image_file)
                w, h = image.size
                image.load()
            res = zbarlight.scan_codes('qrcode', image=image)
            if res is not None:
                self.add_to_list(res)
                color = (90, 209, 66)
                # color = (random.randint(0,256), random.randint(0,256), random.randint(0,256))
                cv2.rectangle(img=frame,
                              pt1=(0, 0),
                              pt2=(w, h),
                              thickness=30,
                              color=color
                              )
                cv2.putText(img=frame,
                            text='Scanned',
                            org=(5, 12),
                            fontFace=cv2.FONT_HERSHEY_PLAIN,
                            fontScale=1,
                            color=(0, 0, 0),
                            thickness=1
                            )
                print(qr_codes)
            cv2.imshow('frame', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cv2.destroyWindow('frame')
        for i in range(5):
            cv2.waitKey(1)

if __name__ == '__main__':
    simple_thread = SimpleTestThread()
    simple_thread.start()
    input()

